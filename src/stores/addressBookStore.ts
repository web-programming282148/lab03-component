import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAddressBookStore = defineStore('address_book', () => {

  interface AddressBook {
    id: number
    name: string
    tel: string
    gender: string
}
const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
})
let lastId = 1
const addressList = ref<AddressBook[]>([])
const isAddNew = ref(false)
function save() {
    if (address.value.id > 0) {
        //Edit
        const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
        addressList.value[editedIndex] = address.value
    } else {
        //AddNew
        addressList.value.push({ ...address.value, id: lastId++ })
    }

    address.value = {
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
    }
}
function cancle(){
  isAddNew.value = false
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

function edit(id: number) {
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))

}
function remove(id: number) {
    const removeIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(removeIndex, 1)

}


  return { address, addressList, save, isAddNew, edit, remove, cancle }
})
